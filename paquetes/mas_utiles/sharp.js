//trabajar con imagenes

const sharp = require('sharp')

sharp('./sharp.png')
    .resize(80)
    .grayscale()
    .toFile('resized.png')
    // La siguiente reducira una imagen de 120x120 o cualquier tamaño a 80x80 y 
    //lo guardara en una imagen mas pequeña sin eliminar la original.