function hola(nombre,callback){
    setTimeout( ( ) => {
        console.log('hola',nombre)
        callback(nombre)
        //el primer parametro será el argumento de la funcion
    },1000)
}

function conversacion(dialogo,callbackFinal){
    setTimeout( () => {
        console.log(dialogo);
        callbackFinal("parametro conversacion")
    },1100 )
}


function adios (nombre, otrocallback){
    setTimeout( () => {
        console.log("adiuss "+nombre);
        otrocallback()
    },900 )
}

/* hola('anderson', function(nombre){
    conversacion('como te fue hoy...blablabla oh que bien peroooooo', function (nombre2) {
        adios(nombre, function (){
            console.log('terminando proceso '+nombre2);
            callbackhell
        })
    } )
} ) */


function conversacionRecursive(nombre,veces,callback){
    if(veces > 0){
        conversacion('bla bla bla bla', function(){

            conversacionRecursive(nombre,--veces , callback)
        })
    }else{
        adios(nombre, callback)
    }
}
hola('anderson',function(nombre){
    conversacionRecursive(nombre,4,function(){
        console.log('proceso terminado')
    })
})