//modulo de procesos hijos
const { exec,spawn } = require('child_process');

/* exec('ls -lha',(err,stdout,sterr) => {
    //stout = standar output
    //standard error
    if(err){
        console.error(err);
        return false
    };

    console.log(stdout)
}) */

/* exec('node modulos/consola.js ',(err,stdout,sterr) => {
    //stout = standar output
    //standard error
    if(err){
        console.error(err);
        return false
    };
    //este es un ejemplo de un archivo node ejecutando otro proceso node
    console.log(stdout)
}) */

let proceso = spawn('ls',[ '-lhs' ]);

console.log("Id del proceso "+proceso.pid);
console.log(proceso.connected)

//utilizado el estandar output
proceso.stdout.on('data', function(dato) {
    console.log('esta muerto?');
    console.log(proceso.killed);
    console.log(dato.toString())
} );

proceso.on('exit', function(){
    console.log('el proceso termino')
    console.log(proceso.killed)
} )