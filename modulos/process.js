/* const process = require('process'); 
NO es necesario de importar ya que podemos utilizarlo desde el process global que se encuentra en los modulos globales

*/

process.on('beforeExit', () => {
    console.log('El proceso va a terminar');
    
})

process.on('exit', () => {
    console.log('El proceso acabó');
    //ya aqui nos hemos desconectado del eventLoop
    
})

//escchar cuando algo se rompe
process.on('uncaughtException', (err,process)=>{
    console.error('Vaya, se nos ha olvidado capturar un error');
    console.error(err);
    //podemos utlizar el modulo de filesystem y el error para guardarlo en un archivo de errorres, o cualquier otra cosa con el 
})

/* functionQuenoExiste() *///esto es capturado por el uncaughtException. Una funcion que no existe


console.log('esto si el error no se atrapa no sale');


/* process.on('unhandledRejection') *///para promesas con rechazadas cuando nadie tiene un cath
