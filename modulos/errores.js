function serompe() {
    return 3+ z;
}

function otrafuncion(){
    serompe()
}

function serompeAsync(callback){
    setTimeout( function(){
        try {
            return 3+z

        } catch (err) {
            console.error('error en mi funcion asincrona')
            callback(err)
        }
    } )
}


try {
    /* otrafuncion(); */
    serompeAsync(  function(err){
        console.log(err.message)
    } )
    //forma para capturar un error de un funcion asincrona

} catch (err){
    console.error('Algo salio mal')
    console.error(err)
    console.log('Lo hemos capturado, el codigo sigue corriendo')
    //hay que saber que hacer con este erro para saberlo manejar
}
//la idea es hacer esta practica con cada funcion que sepamos/sospechemos que puede romperse en un momento dado