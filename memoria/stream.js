const fs= require('fs');
const stream = require('stream');
const util = require('util');//Funcion para poder trabajar con hetencia directv sin peos


//los stream son canales de transmision de informacion. Se utiliza para informacion que es muy grande o que desconocemos su tamaño
//Un ejemplo claro de esto son las promesas que desconocemos el tamaño de la respuesta, subidas de videos o de archivos muy grandes
let data = '';

let readableStream = fs.createReadStream(__dirname + '/input.txt' );

/* readableStream.setEncoding('UTF8')//para no tener que hacer el toString al saber de antemano que tipo de archivo viene 
readableStream.on('data', function(chunk){
    console.log(chunk);
    data+= chunk;
    //si el archivo es muy grande aqui vamos a ir agregando a data mediante se vaya leyendo
}); */


/* readableStream.on('end', ()=>{
    console.log(data);
    //para cuando termine de cargarse la data desde el stream
}) */

//este era un stream de LECTURA


//------- De ESCRITURA

/* process.stdout.write('Hola') */// esto es escribir en el buffer de salida de el sistema
/* process.stdout.write('Que')
process.stdout.write('Tal') */

//------- De ESCRITURA

// Buffer INTERMEDIO/de TRANSFORMACION para leer y escribir

const Transform = stream.Transform; //Esto crea un stream que puede tanto ler como escribir

function Mayus(){
    Transform.call(this)
    //Esto llama al constructor de la clase padre
}

util.inherits(Mayus,Transform)
 //Lo que le indicamos es qu tra lo que necesitamos de mayus de la clase Transform

 Mayus.prototype._transform = function ( chunk, codif, callback ){
     chunkMayus = chunk.toString().toUpperCase()
     this.push(chunkMayus)
     callback()
 }

let mayus = new Mayus()


readableStream
    .pipe(mayus)
    .pipe(process.stdout);
    //pipe se utiliza para pasar informacion de un sitio a otro