
let suma = 0;
console.time('bucle')
for (let index = 0; index < 100000000; index++) {
    suma+=1
}
console.timeEnd('bucle')

console.time('Asincrona')
asincrona()
    .then( ()=>{
        console.timeEnd('Asincrona')
    })



let suma2 = 0;
console.time('bucle2')
for (let j = 0; j < 100000000; j++) {
    suma2+=1
}
console.timeEnd('bucle2')

function asincrona(){
    return new Promise( (resolve,reject)=>{
        setTimeout(()=>{
            console.log('soy Asincrona ');
            resolve()
        },500)
    })
}

//Podemos saber cuando tarda en ejecutarse un fragmento de nuestro codigo o todo el archivo si se desea