async function hola(nombre){
    return new Promise( (resolve,reject)=>{
        setTimeout( ( ) => {
            console.log('hola '+nombre)
            resolve(nombre)
            //el primer parametro será el argumento de la funcion
        },1000)

    } )
}

async function conversacion(){
    return new Promise( function(resolve,reject) {
        setTimeout( function() {
            console.log('bla bla bla bla bla')
            resolve('bla bla bla bla bla')
        },1100 );
    });
}

async function adios (nombre){
    return new Promise( ( resolve,reject ) => {
        setTimeout( () => {
            console.log("adiuss "+nombre);
            resolve()
        },900 )

    } )
}

//-- Process
async function main(){
    let nombre = await hola('Anderson'); //atrapa el valor que viene en el resolve
    await conversacion()
    await conversacion()
    await conversacion()
    await adios(nombre)
}

main()