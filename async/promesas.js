        //en vez de dovolver callback se usara promesa

function hola(nombre){
    return new Promise( (resolve,reject)=>{
        setTimeout( ( ) => {
            console.log('dentro de saludo')
            console.log('hola',nombre)
            resolve(nombre)
            console.log('se acabo el saludo')
            //el primer parametro será el argumento de la funcion
        },1000)

    } )
}

function conversacion(dialogo){
    return new Promise( function(resolve,reject) {
        setTimeout( function() {
            console.log('dentro de conversacion');
            console.log(dialogo);
            resolve('blublublu')
        },1100 );
    });
}

function adios (nombre){
    return new Promise( ( resolve,reject ) => {
        setTimeout( () => {
            console.log("adiuss "+nombre);
            resolve()
        },900 )

    } )
}

hola('Anderson')
    .then(conversacion)
    .then(conversacion)
    .then(conversacion)
    .then(adios)
    .then(nombre=>{
        console.log('terminando proceso')
    })
    .catch(error =>{
        console.log('se peto con el error:',error)
    })