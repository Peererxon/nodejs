const { app,BrowserWindow } = require('electron')

let mainWindow;


app.on('ready', createWindow() )//para que cuando app termine de preparar los procesos para una aplicacion cree la ventana
//este proceso de app ocurre automaticamente al importar app desde electron

function createWindow(){
    mainWindow = new BrowserWindow({
        width:800,
        height:600,
       /*webPreferences: {
            preload: null
            esto seria para darle preferencias
        } */
    });

    mainWindow.loadFile('index.html')
}