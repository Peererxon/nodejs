//aqui vamos a generar todo desde puppeteer
//puppeteer funciona totalmente de forma asincrona
const puppeteer = require('puppeteer');

//-- funciones autoejecutadas
(async () =>{
//Nuestro codigo

    console.log('Lanzamos navegador!')
    console.time('Abriendo_navegador')

    /* const browser = await puppeteer.launch(); */
    const browser = await puppeteer.launch({ headless:false });// con esto le decimos que aparezcaaa!!!
    console.timeEnd('Abriendo_navegador')
    
    const page = await browser.newPage()
    await page.goto('https://es.wikipedia.org/wiki/Node.js')
    
    var titulo1 = await page.evaluate(()=>{
        const h1 = document.querySelector('h1');
        console.log(h1.innerHTML)
        return h1.innerHTML
    })//ejecuta un script 

    console.log(titulo1);
    


    console.log('cerramos navegador....')

    browser.close();
    console.log(' navegador cerrado')
})();

//Esto es un ejmplo secillo de como hacer scrapping con nodejs para recolectar informacion de una pagina web